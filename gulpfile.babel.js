import gulp from 'gulp';
import gulpBabel from 'gulp-babel';
import browserify from 'browserify';
import babelify from 'babelify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';

gulp.task('default', [ 'build' ]);

gulp.task('build', () => {
  let bundler = browserify('./src/client.js', { debug: true }).transform(babelify);
  return bundler.bundle()
    .on('error', console.error)
    .pipe(source('client.js'))
    .pipe(buffer())
    .pipe(gulp.dest('static'));
});
